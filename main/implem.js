    var userChoice = prompt("pierre, feuille ou ciseaux ?");

    document.write("<p>Votre choix :" + " " + userChoice + "</p>");

    var computerChoice = Math.random();
    if (computerChoice < 0.34) {
        computerChoice = "pierre";
    } else if (computerChoice <= 0.67) {
        computerChoice = "feuille";
    } else {
        computerChoice = "ciseaux";
    }

    document.write("<p>Ordinateur :" + " " + computerChoice + "</p>");

    var compare = function (choice1, choice2) {
        if (choice1 === choice2) {
            return "Egalité !";
        }
        if (choice1 === "pierre") {
            if (choice2 === "ciseaux") {
                return "C'est gagné !";
            } else {
                return "C'est un échec, comme l'ensemble de votre vie, retentez votre chance :)";
            }
        }
        if (choice1 === "feuille") {
            if (choice2 === "pierre") {
                return "C'est gagné !";
            } else {
                return "C'est un échec, comme l'ensemble de votre vie, retentez votre chance :)";
            }
        }
        if (choice1 === "ciseaux") {
            if (choice2 === "pierre") {
                return "C'est un échec, comme l'ensemble de votre vie, retentez votre chance :)";
            } else {
                return "C'est gagné !";
            }
        }
    };

    var results = compare(userChoice, computerChoice);

    document.write("<br><hr><br>" + results);
